<?php
    use yii\helpers\Url;
?>

<div class="col-md-4" data-sticky_column>
    <div class="primary-sidebar">

        <aside class="widget">
            <h3 class="widget-title text-uppercase text-center">Popular Posts</h3>

            <?php

            foreach ($popular as $article):?>
                <div class="popular-post">


                    <a href="<?= Url::toRoute(['site/article', 'id'=>$article->id]);?>" class="popular-img"><img src="<?= $article->getImage(); ?>" alt="">

                        <div class="p-overlay"></div>
                    </a>

                    <div class="p-content">
                        <a href="<?= Url::toRoute(['site/article', 'id'=>$article->id]);?>" class="text-uppercase"><?= $article->title ?></a>
                        <span class="p-date"><?= $article->date ?></span>
                    </div>
                </div>
            <?php endforeach; ?>

        </aside>
        <aside class="widget pos-padding">
            <h3 class="widget-title text-uppercase text-center">Recent Posts</h3>

            <?php foreach ($recent as $article):?>
                <div class="thumb-latest-posts">


                    <div class="media">
                        <div class="media-left">
                            <a href="<?= Url::toRoute(['site/article', 'id'=>$article->id]);?>" class="popular-img"><img src="<?= $article->getImage(); ?>" alt="">
                                <div class="p-overlay"></div>
                            </a>
                        </div>
                        <div class="p-content">
                            <a href="<?= Url::toRoute(['site/article', 'id'=>$article->id]);?>" class="text-uppercase"><?= $article->title ?></a>
                            <span class="p-date"><?= $article->date ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </aside>


        <!--      <aside class="widget border pos-padding">
                  <h3 class="widget-title text-uppercase text-center">Categories</h3>
                  <ul>
                      <li>
                          <a href="#">Food & Drinks</a>
                          <span class="post-count pull-right"> (2)</span>
                      </li>
                      <li>
                          <a href="#">Travel</a>
                          <span class="post-count pull-right"> (2)</span>
                      </li>
                      <li>
                          <a href="#">Business</a>
                          <span class="post-count pull-right"> (2)</span>
                      </li>
                      <li>
                          <a href="#">Story</a>
                          <span class="post-count pull-right"> (2)</span>
                      </li>
                      <li>
                          <a href="#">DIY & Tips</a>
                          <span class="post-count pull-right"> (2)</span>
                      </li>
                      <li>
                          <a href="#">Lifestyle</a>
                          <span class="post-count pull-right"> (2)</span>
                      </li>
                  </ul>
              </aside> -->
    </div>
</div>