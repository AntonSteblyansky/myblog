<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model{

    public $image;

    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg,png']
        ];
    }

    /**
     * @param UploadedFile $file
     * @param $currentImage
     * @return string
     */
    public  function uploadFile(UploadedFile $file, $currentImage)
    {
        //??????
        if($this->validate()){ die("fsdfdfsdf");}

            $this->deleteCurrentImage($currentImage);
            return $this->saveImage($file);
       // }

    }


    public function deleteCurrentImage($currentImage){

        if($this->fileExiste($currentImage)){
            unlink($this->getFolder() . $currentImage);
        }
    }

    private function fileExiste($currentImage){
        if (!empty($currentImage) && $currentImage != null) {
            return file_exists($this->getFolder() . $currentImage);
        }
    }

    private function saveImage($file){

        $fileName = $this->generateFileName($file);
        $file->saveAs($this->getFolder() . $fileName);

        return $fileName;
    }

    private function generateFileName($file){
        return md5(uniqid($file->baseName)). '.' . $file->extension;
    }

    private function getFolder(){
        return Yii::getAlias('@web') . 'uploads/';
    }
}